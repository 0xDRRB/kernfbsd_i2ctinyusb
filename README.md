See `i2ctinyusb.4`

Note (2024/04/21): This code has now been [merged](https://github.com/freebsd/freebsd-src/pull/1123) into the official [FreeBSD sources](https://github.com/freebsd/freebsd-src/tree/main).
